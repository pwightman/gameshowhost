
// To check if a library is compiled with CocoaPods you
// can use the `COCOAPODS` macro definition which is
// defined in the xcconfigs so it is available in
// headers also when they are imported in the client
// project.


// DTBonjour
#define COCOAPODS_POD_AVAILABLE_DTBonjour
#define COCOAPODS_VERSION_MAJOR_DTBonjour 1
#define COCOAPODS_VERSION_MINOR_DTBonjour 0
#define COCOAPODS_VERSION_PATCH_DTBonjour 0

// MTBlockTableView
#define COCOAPODS_POD_AVAILABLE_MTBlockTableView
#define COCOAPODS_VERSION_MAJOR_MTBlockTableView 0
#define COCOAPODS_VERSION_MINOR_MTBlockTableView 0
#define COCOAPODS_VERSION_PATCH_MTBlockTableView 1

// MTControl
#define COCOAPODS_POD_AVAILABLE_MTControl
#define COCOAPODS_VERSION_MAJOR_MTControl 0
#define COCOAPODS_VERSION_MINOR_MTControl 0
#define COCOAPODS_VERSION_PATCH_MTControl 4

// PSAlertView
#define COCOAPODS_POD_AVAILABLE_PSAlertView
#define COCOAPODS_VERSION_MAJOR_PSAlertView 1
#define COCOAPODS_VERSION_MINOR_PSAlertView 1
#define COCOAPODS_VERSION_PATCH_PSAlertView 0

// SVHTTPRequest
#define COCOAPODS_POD_AVAILABLE_SVHTTPRequest
#define COCOAPODS_VERSION_MAJOR_SVHTTPRequest 0
#define COCOAPODS_VERSION_MINOR_SVHTTPRequest 4
#define COCOAPODS_VERSION_PATCH_SVHTTPRequest 0

