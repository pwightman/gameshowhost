//
//  GSQuestionPackTests.m
//  GameShowHost
//
//  Created by Parker Wightman on 3/13/13.
//  Copyright (c) 2013 Parker Wightman. All rights reserved.
//

#import "GSQuestionPackTests.h"

#import "GSQuestionPack.h"
#import "GSQuestion.h"

@implementation GSQuestionPackTests

- (void) setUp {
    
}

- (void) tearDown {
    
}

- (void) testDeserializing {
    NSString *jsonPath = [[NSBundle mainBundle] pathForResource:@"file_format" ofType:@"json"];
    NSData *json = [NSData dataWithContentsOfFile:jsonPath];
    
    NSError *error = nil;
    NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:json options:0 error:&error];
    
    if ( error ) {
        STFail(@"file_format.json did not parse correctly: %@", error.localizedDescription);
    }
    
    GSQuestionPack *questionPack = [GSQuestionPack questionPackFromJSON:dict];
    
    STAssertTrue([questionPack.name isEqualToString:[dict objectForKey:@"name"]], nil);
    STAssertTrue([questionPack.formatVersion isEqualToString:[dict objectForKey:@"format_version"]], nil);
    STAssertTrue(questionPack.categoriesCount == [[dict objectForKey:@"categories_count"] integerValue], nil);
    STAssertTrue(questionPack.questionsPerCategoryCount == [[dict objectForKey:@"questions_per_category_count"] integerValue], nil);
    STAssertTrue([questionPack.formatVersion isEqualToString:[dict objectForKey:@"format_version"]], nil);
    STAssertTrue([questionPack.dateUpdated isEqualToString:[dict objectForKey:@"updated_at"]], nil);
    STAssertTrue(questionPack.categoriesCount == [[dict objectForKey:@"categories"] count], nil);
    STAssertTrue(questionPack.categories.count == [[dict objectForKey:@"categories"] count], nil);
    STAssertTrue(questionPack.questionsPerCategoryCount == [[dict objectForKey:@"questions_per_category_count"] integerValue], nil);
    
    NSArray *categories = [dict objectForKey:@"categories"];
    
    for (NSInteger i = 0; i < questionPack.categoriesCount; i++) {
        STAssertTrue([[[questionPack.categories objectAtIndex:i] name] isEqualToString:[[categories objectAtIndex:i] objectForKey:@"category"]], nil);
    }
    
    for (NSInteger i = 0; i < questionPack.categoriesCount; i++) {
        STAssertTrue([[questionPack.questions objectAtIndex:i] count] == questionPack.questionsPerCategoryCount, nil);
        NSArray *questions = [[categories objectAtIndex:i] objectForKey:@"questions"];
        NSArray *nativeQuestions = [questionPack.questions objectAtIndex:i];
        for (NSInteger j = 0; j < questionPack.questionsPerCategoryCount; j++) {
            NSDictionary *question = [questions objectAtIndex:j];
            GSQuestion *nativeQuestion = [nativeQuestions objectAtIndex:j];
            
            STAssertTrue([[question objectForKey:@"question"] isEqualToString:nativeQuestion.question], nil);
            STAssertTrue([[question objectForKey:@"answer"] isEqualToString:nativeQuestion.answer], nil);
            STAssertTrue([[question objectForKey:@"value"] integerValue] == nativeQuestion.value, nil);
        }
    }
    
}

- (void) testSerializing {
    NSString *jsonPath = [[NSBundle mainBundle] pathForResource:@"file_format" ofType:@"json"];
    NSData *json = [NSData dataWithContentsOfFile:jsonPath];
    
    NSError *error = nil;
    NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:json options:0 error:&error];
    
    GSQuestionPack *questionPack = [GSQuestionPack questionPackFromJSON:dict];
    
    NSDictionary *outDict = [questionPack toJSON];
    
    STAssertTrue([outDict isEqualToDictionary:dict], nil);
}

@end
