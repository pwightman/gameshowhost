//
//  GSCategory.h
//  GameShowHost
//
//  Created by Parker Wightman on 3/13/13.
//  Copyright (c) 2013 Parker Wightman. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface GSCategory : NSObject

@property (strong, nonatomic, readonly) NSString *name;

+ (id) categoryWithName:(NSString *)name;

@end
