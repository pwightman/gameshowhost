//
//  GSExternalInGameViewController.m
//  GameShowHost
//
//  Created by Parker Wightman on 3/14/13.
//  Copyright (c) 2013 Parker Wightman. All rights reserved.
//

#import "GSExternalInGameViewController.h"

@interface GSExternalInGameViewController ()

@property (strong, nonatomic) GSBoardView *boardView;
@property (strong, nonatomic) UIView *visibleQuestion;

@end

@implementation GSExternalInGameViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    _boardView = [[GSBoardView alloc] initWithBoard:_board];
    
    [self.view addSubview:_boardView];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    _boardView.frame = self.view.frame;
}

- (void) loadNewBoard:(GSBoard *)board {
    _board = board;
    _boardView.board = _board;
    [_boardView setNeedsLayout];
}

- (void) selectQuestion:(NSInteger)question inCategory:(NSInteger)category {
    GSQuestion *q = [[_board.questionPack.questions objectAtIndex:category] objectAtIndex:question];
    
    UIView *view = [[UIView alloc] initWithFrame:self.view.bounds];
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectInset(view.bounds, 10, 10)];
    label.text = q.question;
    label.font = [UIFont appFontWithSize:60];
    label.numberOfLines = 8;
    label.minimumScaleFactor = 0.01;
    [label adjustFontSizeToFit];
    label.textColor = [UIColor whiteColor];
    label.shadowColor = [UIColor blackColor];
    label.shadowOffset = CGSizeMake(1, 1);
    label.textAlignment = NSTextAlignmentCenter;
    
    [view addSubview:label];
    _visibleQuestion = view;
    [self.view addSubview:view];
    
    CGFloat width = _boardView.bounds.size.width / _board.questionPack.categoriesCount;
    CGFloat height = _boardView.bounds.size.height / (_board.questionPack.questionsPerCategoryCount + 1);
    CGFloat x = (category * width);
    CGFloat y = ((question + 1) * height);
    
    CGRect frame = CGRectMake(x, y, _boardView.bounds.size.width, _boardView.bounds.size.height);
    view.frame = frame;
    
    view.center = CGPointMake(x + width/2, y + height/2);
    
    CGFloat ratio = width/_boardView.bounds.size.width;
    
    view.transform = CGAffineTransformMakeScale(ratio, ratio);
    
    [UIView animateWithDuration:1.0
                          delay:0.1
                        options:0
                     animations:^{
                         view.transform = CGAffineTransformIdentity;
                         view.frame = self.view.bounds;
                     } completion:^(BOOL finished) {}];
    
    view.backgroundColor = [UIColor blueColor];
    label.backgroundColor = [UIColor clearColor];
    
    
}

- (void) playerBuzzedIn:(GSPlayer *)player {
    
}
    
- (void) playerAnswered:(BOOL)isCorrect {
    
}
    
- (void) showAnswer {
    
}

- (void) finishQuestion {
    [_boardView setNeedsLayout];
    CGRect frame = _visibleQuestion.frame;
    frame.origin.x = self.view.bounds.size.width;
    [UIView animateWithDuration:1.0
                          delay:0
                        options:0
                     animations:^{
                         _visibleQuestion.frame = frame;
                         CGAffineTransform trans = CGAffineTransformConcat(CGAffineTransformMakeScale(0, 0), CGAffineTransformMakeRotation(M_PI));
                         _visibleQuestion.transform = trans;
                     } completion:^(BOOL finished) {
                         [_visibleQuestion removeFromSuperview];
                     }];
}

- (void) showScores:(NSArray *)players {
    
}

- (void) buzzingBegan {
    _visibleQuestion.backgroundColor = [UIColor greenColor];
}

- (void) buzzingEnded {
    _visibleQuestion.backgroundColor = [UIColor redColor];
}

@end
