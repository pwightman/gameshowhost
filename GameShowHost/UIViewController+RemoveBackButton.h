//
//  UIViewController+RemoveBackButton.h
//  GameShowHost
//
//  Created by Parker Wightman on 3/14/13.
//  Copyright (c) 2013 Parker Wightman. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIViewController (RemoveBackButton)

- (void) replaceBackArrowWithBarButtonItem:(UIBarButtonItem *)item;

@end
