//
//  GSGameEndBuzzingView.m
//  GameShowHost
//
//  Created by Parker Wightman on 3/13/13.
//  Copyright (c) 2013 Parker Wightman. All rights reserved.
//

#import "GSGameEndBuzzingView.h"

@implementation GSGameEndBuzzingView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        _endBuzzingButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
        [_endBuzzingButton setTitle:@"End Buzzing" forState:UIControlStateNormal];
        _endBuzzingButton.titleLabel.font = [UIFont appFontWithSize:50];
        [self addSubview:_endBuzzingButton];
    }
    return self;
}

- (void) layoutSubviews {
    _endBuzzingButton.frame = self.bounds;
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
