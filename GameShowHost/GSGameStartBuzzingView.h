//
//  GSGameStartBuzzingView.h
//  GameShowHost
//
//  Created by Parker Wightman on 3/13/13.
//  Copyright (c) 2013 Parker Wightman. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GSGameStartBuzzingView : UIView

@property (strong, nonatomic) UIButton *startBuzzingButton;

@end
