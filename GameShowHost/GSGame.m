//
//  GSGame.m
//  GameShowHost
//
//  Created by Parker Wightman on 3/13/13.
//  Copyright (c) 2013 Parker Wightman. All rights reserved.
//

#import "GSGame.h"

@implementation GSGame

- (void) broadcastPlayersUpdate {
    for (GSPlayer *player in _players) {
        [player.notifier sendPlayersUpdate:_players];
    }
}

- (void) broadcastAndChangeScore:(NSInteger)newScore forPlayer:(GSPlayer *)player {
    NSLog(@"Broadcasting player %@ score from: %@ to %@", player.name, @(player.score), @(newScore));
    player.score = newScore;
    [self broadcastPlayersUpdate];
}

@end
