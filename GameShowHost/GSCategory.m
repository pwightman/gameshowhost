//
//  GSCategory.m
//  GameShowHost
//
//  Created by Parker Wightman on 3/13/13.
//  Copyright (c) 2013 Parker Wightman. All rights reserved.
//

#import "GSCategory.h"

@implementation GSCategory

+ (id) categoryWithName:(NSString *)name {
    GSCategory *category = [[GSCategory alloc] init];
    
    category->_name = name;
    
    return category;
}

@end
