//
//  GSBoard.h
//  GameShowHost
//
//  Created by Parker Wightman on 3/13/13.
//  Copyright (c) 2013 Parker Wightman. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "GSQuestionPack.h"

@interface GSBoard : NSObject

@property (strong, nonatomic) GSQuestionPack *questionPack;
@property (strong, nonatomic) NSMutableArray *playedQuestions;

+ (id) boardWithQuestionPack:(GSQuestionPack *)questionPack;

@end
