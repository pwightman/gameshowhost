//
//  GSBoardCell.h
//  GameShowHost
//
//  Created by Parker Wightman on 3/13/13.
//  Copyright (c) 2013 Parker Wightman. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GSBoardCell : UIControl

@property (strong, nonatomic) UILabel *label;
@property (assign, nonatomic) NSInteger column;
@property (assign, nonatomic) NSInteger row;

@end
