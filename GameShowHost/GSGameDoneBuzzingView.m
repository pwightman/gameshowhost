//
//  GSGameDoneBuzzingView.m
//  GameShowHost
//
//  Created by Parker Wightman on 3/13/13.
//  Copyright (c) 2013 Parker Wightman. All rights reserved.
//

#import "GSGameDoneBuzzingView.h"

@implementation GSGameDoneBuzzingView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        _continueButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
        _showAnswerButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
        
        [_continueButton setTitle:@"Continue" forState:UIControlStateNormal];
        _continueButton.titleLabel.font = [UIFont appFontWithSize:50];
        
        [_showAnswerButton setTitle:@"Show Answer" forState:UIControlStateNormal];
        _showAnswerButton.titleLabel.font = [UIFont appFontWithSize:50];
        
        [self addSubview:_continueButton];
        [self addSubview:_showAnswerButton];
    }
    return self;
}

- (void) layoutSubviews {
    CGFloat width = self.frame.size.width / 2;
    CGFloat height = self.frame.size.height;
    _showAnswerButton.frame = CGRectMake(width * 0, 0, width, height);
    _continueButton.frame   = CGRectMake(width * 1, 0, width, height);
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
