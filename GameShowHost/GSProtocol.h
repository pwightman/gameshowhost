//
//  GSProtocol.h
//  GameShowHost
//
//  Created by Parker Wightman on 3/12/13.
//  Copyright (c) 2013 Parker Wightman. All rights reserved.
//

#ifndef GameShowHost_GSProtocol_h
#define GameShowHost_GSProtocol_h

typedef NS_ENUM(NSInteger, GSProtocol) {
    GSProtocolSignInRequest          = 0,
    GSProtocolTeamNameRequest        = 1,
    GSProtocolSignedIn               = 2,
    GSProtocolSignOut                = 3,
    GSProtocolGameBegan              = 4,
    GSProtocolPlayersUpdate          = 5,
    GSProtocolChoosingQuestion       = 6,
    GSProtocolQuestionChosen         = 7,
    GSProtocolBeginBuzzing           = 8,
    GSProtocolBuzzIn                 = 9,
    GSProtocolBuzzedEarly            = 10,
    GSProtocolBuzzedIn               = 11,
    GSProtocolWonBuzzIn              = 12
};

#endif
