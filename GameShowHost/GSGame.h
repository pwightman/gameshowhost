//
//  GSGame.h
//  GameShowHost
//
//  Created by Parker Wightman on 3/13/13.
//  Copyright (c) 2013 Parker Wightman. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "GSBoard.h"
#import "GSGameOptions.h"
#import "GSPlayer.h"


@interface GSGame : NSObject

@property (strong, nonatomic) GSGameOptions *gameOptions;
@property (strong, nonatomic) NSArray *players;
@property (strong, nonatomic) GSBoard *board;

- (void) broadcastPlayersUpdate;
- (void) broadcastAndChangeScore:(NSInteger)newScore forPlayer:(GSPlayer *)player;

@end
