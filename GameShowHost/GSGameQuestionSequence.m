//
//  GSGameQuestionSequence.m
//  GameShowHost
//
//  Created by Parker Wightman on 3/13/13.
//  Copyright (c) 2013 Parker Wightman. All rights reserved.
//

#import "GSGameQuestionSequence.h"
#import <QuartzCore/QuartzCore.h>
#import "GSPlayer.h"

#define PENALTY_TIME 3.0

@interface GSGameQuestionSequence () <GSPlayerDelegate>

@property (strong, nonatomic) NSTimer *questionTimer;
@property (strong, nonatomic) NSTimer *playerTimer;
@property (assign, nonatomic) CGFloat durationLeft;
@property (assign, nonatomic) CGFloat timeQuestionTimerBegan;
@property (strong, nonatomic) GSPlayer *answeringPlayer;

@property (strong, nonatomic) NSMutableSet *playersBuzzedInEarly;
@property (strong, nonatomic) NSMutableSet *playersAnsweredIncorrectly;

@end

@implementation GSGameQuestionSequence

+ (id)questionSequenceWithGame:(GSGame *)game
                      category:(GSCategory *)category
                      question:(GSQuestion *)question {
    
    GSGameQuestionSequence *sequence = [[GSGameQuestionSequence alloc] initWithGame:game
                                                                           category:category
                                                                           question:question];
    
    return sequence;
}

- (id) initWithGame:(GSGame *)game
           category:(GSCategory *)category
           question:(GSQuestion *)question {
    self = [super init];
    
    if ( self ) {
        _game = game;
        _category = category;
        _question = question;
        
        for (GSPlayer *player in game.players) {
            player.delegate = self;
        }
        
        _state = GSGameQuestionSequenceStateReading;
        _durationLeft = _game.gameOptions.timerDuration;
        _playersAnsweredIncorrectly = [NSMutableSet set];
        _playersBuzzedInEarly = [NSMutableSet set];
    }
    
    return self;
}


- (void) playerAnswered:(BOOL)isCorrect {
    [_playerTimer invalidate];
    if ( isCorrect ) {
        _playerWithBoard = _answeringPlayer;
        
        NSInteger newScore = _answeringPlayer.score + _question.value;
        [_game broadcastAndChangeScore:newScore forPlayer:_answeringPlayer];
        
        [self transitionTo:GSGameQuestionSequenceStateDoneBuzzing];
    } else {
        NSInteger newScore = _answeringPlayer.score - _question.value;
        [_game broadcastAndChangeScore:newScore forPlayer:_answeringPlayer];
        
        [_playersAnsweredIncorrectly addObject:_answeringPlayer];
        [self startBuzzing];
    }
}

- (void) startBuzzing {
    if ( _state == GSGameQuestionSequenceStateReading  || _state == GSGameQuestionSequenceStateBuzzedIn ) {
        if ( _durationLeft < 0 ) {
            [self questionTimerUp];
            return;
        }
        
        NSLog(@"Starting question timer with duration: %f", _durationLeft);
        
        [self transitionTo:GSGameQuestionSequenceStateWaitingForBuzz];
        
        if ( _game.gameOptions.timerDuration > 0.0) {
            _questionTimer = [NSTimer scheduledTimerWithTimeInterval:_durationLeft
                                                              target:self
                                                            selector:@selector(questionTimerUp)
                                                            userInfo:nil
                                                             repeats:NO];
            
            if ( [_delegate respondsToSelector:@selector(questionSequence:shouldStartTimerWithDuration:)] ) {
                [_delegate questionSequence:self shouldStartTimerWithDuration:_durationLeft];
            }
        }
        
        _timeQuestionTimerBegan = CACurrentMediaTime();
    }
}

- (void) questionTimerUp {
    NSLog(@"Question timer ran out");
    [_playerTimer invalidate];
    [self transitionTo:GSGameQuestionSequenceStateDoneBuzzing];
}

- (void) playerTimerUp {
    NSLog(@"Player timer ran out");
    [self playerAnswered:NO];
}

- (void) endBuzzing {
    if ( _state == GSGameQuestionSequenceStateWaitingForBuzz ) {
        [_questionTimer invalidate];
        [self transitionTo:GSGameQuestionSequenceStateDoneBuzzing];
    }
}

- (void) transitionTo:(GSGameQuestionSequenceState)state {
    [_delegate questionSequence:self willTransitionToState:state fromState:_state];
    _state = state;
}


#pragma mark GSPlayerDelegate

- (void) playerDidBuzzIn:(GSPlayer *)player {
    @synchronized(self) {
        NSLog(@"Got buzz in");
        if ( _state == GSGameQuestionSequenceStateReading ){
            if ( ![_playersBuzzedInEarly containsObject:player] )
                [_delegate questionSequence:self playerDidBuzzEarly:player];
            [_playersBuzzedInEarly addObject:player];
        } else if ( _state == GSGameQuestionSequenceStateWaitingForBuzz && ![_playersAnsweredIncorrectly containsObject:player]) {
            CGFloat now = CACurrentMediaTime();
            
            if ( [_playersBuzzedInEarly containsObject:player] ) {
                if (now - _timeQuestionTimerBegan < PENALTY_TIME) {
                    return;
                }
            }
            
            // After first person buzzes in, no more penalties even
            // if time isn't up in the question timer
            [_playersBuzzedInEarly removeAllObjects];
            
            _answeringPlayer = player;
            
            [_delegate questionSequence:self playerDidBuzzIn:player];
            
            [self transitionTo:GSGameQuestionSequenceStateBuzzedIn];
            
            [_questionTimer invalidate];
            CGFloat elapsed = now - _timeQuestionTimerBegan;
            NSLog(@"Time elapsed before buzz in: %f", elapsed);
            
            _durationLeft -= elapsed;
            
            NSLog(@"New duration: %f", _durationLeft);
            
            if ( _game.gameOptions.timerDuration > 0.0 ) {
                _playerTimer = [NSTimer scheduledTimerWithTimeInterval:_game.gameOptions.timerDuration * 2
                                                                target:self
                                                              selector:@selector(playerTimerUp)
                                                              userInfo:nil
                                                               repeats:NO];
                
                if ( [_delegate respondsToSelector:@selector(questionSequence:shouldStartTimerWithDuration:)] ) {
                    [_delegate questionSequence:self shouldStartTimerWithDuration:_game.gameOptions.timerDuration * 2];
                }
            }
        }
    }
}

- (void) playerDidDisconnect:(GSPlayer *)player {
    
}

- (void) playerDidSignOut:(GSPlayer *)player {
    
}

@end
