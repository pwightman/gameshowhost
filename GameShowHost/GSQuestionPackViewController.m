//
//  GSQuestionPackViewController.m
//  GameShowHost
//
//  Created by Parker Wightman on 3/18/13.
//  Copyright (c) 2013 Parker Wightman. All rights reserved.
//

#import "GSQuestionPackViewController.h"

@interface GSQuestionPackViewController ()

@property (strong, nonatomic) NSMutableArray *questionPacks;
@property (strong, nonatomic) NSArray *localQuestionPacks;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) NSURLSession *session;

@end

#define QUESTION_PACK_SECTION_REMOTE 0
#define QUESTION_PACK_SECTION_LOCAL  1

@implementation GSQuestionPackViewController

- (void)viewDidLoad
{
    _questionPacks = [NSMutableArray array];
    
    _localQuestionPacks = [self fetchLocalQuestionPacks];
    self.session = [NSURLSession sessionWithConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];

    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:@"http://as-gameshow.herokuapp.com/question_packs.json"]];
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];

    [[self.session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        dispatch_async(dispatch_get_main_queue(), ^{
            if ( error ) {
                PSPDFAlertView *alertView = [[PSPDFAlertView alloc] initWithTitle:@"There was an error"
                                                                          message:error.localizedDescription];
                [alertView setCancelButtonWithTitle:@"OK" block:^{ }];
                [alertView show];
            } else {
                NSArray *JSON = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:nil];
                [self processQuestionPackJSON:JSON];
                [_tableView reloadData];
            }
        });
    }] resume];

    [super viewDidLoad];
	// Do any additional setup after loading the view.
}

- (void) processQuestionPackJSON:(NSArray *)questionPacks {
    for (NSDictionary *json in questionPacks) {
        GSQuestionPack *questionPack = [GSQuestionPack questionPackFromJSON:json];
        [_questionPacks addObject:questionPack];
    }
}

- (IBAction)saveToIPadTapped:(id)sender {
    [self saveRemoteQuestionPacksToDisk];
    _localQuestionPacks = [self fetchLocalQuestionPacks];
    [_tableView reloadData];
}

- (IBAction)clearLocalTapped:(id)sender {
    [self clearLocalQuestionPacks];
    _localQuestionPacks = [self fetchLocalQuestionPacks];
    [_tableView reloadData];
}

- (void) clearLocalQuestionPacks {
    [[NSFileManager defaultManager] removeItemAtPath:[self questionPacksPath]
                                               error:nil];
}

- (NSArray *) fetchLocalQuestionPacks {
    NSArray *questionPackFiles = [[NSFileManager defaultManager] contentsOfDirectoryAtPath:[self questionPacksPath] error:nil];
    
    NSMutableArray *packs = [NSMutableArray array];
    
    for (NSString *packPath in questionPackFiles) {
        NSString *fullPath = [[self questionPacksPath] stringByAppendingPathComponent:packPath];
        GSQuestionPack *pack = [GSQuestionPack questionPackFromContentsOfFileAtPath:fullPath];
        [packs addObject:pack];
    }
    
    return packs;
}

- (NSString *)questionPacksPath {
    NSArray *documentsDirList = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    
    NSString *documentsPath = [documentsDirList objectAtIndex:0];
    
    NSString *questionPacksPath = [documentsPath stringByAppendingPathComponent:@"QuestionPacks"];
    
    return questionPacksPath;
}

- (void) saveRemoteQuestionPacksToDisk {
    
    BOOL isDirectory;
    
    NSString *questionPacksPath = [self questionPacksPath];
    
    if (![[NSFileManager defaultManager] fileExistsAtPath:questionPacksPath isDirectory:&isDirectory]) {
        NSError *error = nil;
        [[NSFileManager defaultManager] createDirectoryAtPath:questionPacksPath
                              withIntermediateDirectories:YES
                                               attributes:nil
                                                    error:&error];
    }
    
    
    
    for (GSQuestionPack *pack in _questionPacks) {
        NSDictionary *dict = [pack toJSON];
        
        NSData *json = [NSJSONSerialization dataWithJSONObject:dict
                                                       options:0
                                                         error:nil];
        
        NSString *packPath = [questionPacksPath stringByAppendingPathComponent:[pack.name stringByAppendingString:@".json"]];
        
        [[NSFileManager defaultManager] createFileAtPath:packPath
                                                contents:json
                                              attributes:nil];
    }
    
}

- (NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (section == QUESTION_PACK_SECTION_LOCAL) {
        return _localQuestionPacks.count;
    } else if (section == QUESTION_PACK_SECTION_REMOTE) {
        return _questionPacks.count;
    }
    
    return 0;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    if ( section == QUESTION_PACK_SECTION_REMOTE ) {
        return @"Remote";
    } else {
        return @"Local";
    }
}

- (NSInteger) numberOfSectionsInTableView:(UITableView *)tableView {
    return 2;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell"];
    
    GSQuestionPack *questionPack;
    
    if (indexPath.section == QUESTION_PACK_SECTION_REMOTE) {
        questionPack = [_questionPacks objectAtIndex:indexPath.row];
    } else if ( indexPath.section == QUESTION_PACK_SECTION_LOCAL ) {
        questionPack = [_localQuestionPacks objectAtIndex:indexPath.row];
    }
    
    cell.textLabel.text = questionPack.name;
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    GSQuestionPack *questionPack;
    if ( indexPath.section == QUESTION_PACK_SECTION_LOCAL ) {
        questionPack = [_localQuestionPacks objectAtIndex:indexPath.row];
    } else if ( indexPath.section == QUESTION_PACK_SECTION_REMOTE ) {
        questionPack = [_questionPacks objectAtIndex:indexPath.row];
    }
    
    if (_didFinishWithQuestionPack)
        _didFinishWithQuestionPack(questionPack);
}

@end
