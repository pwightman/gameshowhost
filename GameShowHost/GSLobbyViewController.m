//
//  GSLobbyViewController.m
//  GameShowHost
//
//  Created by Parker Wightman on 3/11/13.
//  Copyright (c) 2013 Parker Wightman. All rights reserved.
//

#import "GSLobbyViewController.h"
#import "GSLobby.h"
#import "GSGameOptions.h"
#import <MTBlockTableView/MTBlockTableView.h>
#import "GSChooseQuestionViewController.h"

@interface GSLobbyViewController () <GSLobbyDelegate>

@property (strong, nonatomic) GSLobby *lobby;
@property (weak, nonatomic) IBOutlet MTBlockTableView *tableView;
@property (weak, nonatomic) IBOutlet UILabel *playersJoinedLabel;
@property (strong, nonatomic) GSGame *game;

@end

@implementation GSLobbyViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    _lobby = [[GSLobby alloc] init];
    
    _lobby.gameOptions = _gameOptions;
    _lobby.delegate    = self;
    
    _game = [[GSGame alloc] init];
    _game.gameOptions = _gameOptions;
    _game.players = _lobby.players;
    
    [_lobby start];
    
    [self removeBackArrow];
    [self setupTableView];
    [self updatePlayerCountLabel];
}

- (void) removeBackArrow {
    UIBarButtonItem *item = [[UIBarButtonItem alloc] initWithTitle:@"Quit"
                                                             style:UIBarButtonItemStyleBordered
                                                            target:self
                                                            action:@selector(backButtonTapped)];
    
    [self replaceBackArrowWithBarButtonItem:item];
}

- (void) backButtonTapped {
    [_lobby stop];
    _lobby = nil;
    if (_didFinish)
        _didFinish();
}

- (void) setupTableView {
    [_tableView setNumberOfRowsInSectionBlock:^NSInteger(UITableView *tableView, NSInteger section) {
        return _lobby.players.count;
    }];
    
    [_tableView setCellForRowAtIndexPathBlock:^UITableViewCell *(UITableView *tableView, NSIndexPath *indexPath) {
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell"];
        
        cell.textLabel.text = [[_lobby.players objectAtIndex:indexPath.row] name];
        
        return cell;
    }];
    
    [_tableView setTitleForHeaderInSectionBlock:^NSString *(UITableView *tableView, NSInteger section) {
        return @"Teams";
    }];
}

- (void) updatePlayerCountLabel {
    _playersJoinedLabel.text = [NSString stringWithFormat:@"%@ players joined...", @(_lobby.players.count)];
}


- (void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ( [segue.identifier isEqualToString:@"startGame"] ) {
        GSChooseQuestionViewController *controller = (GSChooseQuestionViewController *)segue.destinationViewController;
        
        _game.board = [GSBoard boardWithQuestionPack:_gameOptions.questionPack];
        
        
        controller.game = _game;
        GSExternalInGameViewController *inGameController = [self.storyboard instantiateViewControllerWithIdentifier:STORYBOARD_ID_EXTERNAL_IN_GAME];
        inGameController.board = _game.board;
        
        [_externalLobbyController.navigationController pushViewController:inGameController animated:YES];
        controller.externalInGameController = inGameController;
        
        controller.didFinish = ^{
            // Skip the lobby!
            [_lobby stop];
            [_externalLobbyController.navigationController popViewControllerAnimated:NO];
            [_externalLobbyController.navigationController popViewControllerAnimated:YES];
            [self.navigationController popViewControllerAnimated:NO];
            [self.navigationController popViewControllerAnimated:YES];
        };
    }
}



- (void) lobby:(GSLobby *)lobby failedToStartServer:(NSError *)error {
    NSLog(@"Server failed to start...");
}

- (void) lobbyDidStartServer:(GSLobby *)lobby {
    NSLog(@"Server started...");
}

- (void) lobby:(GSLobby *)lobby playerDidJoin:(GSPlayer *)player {
    [_tableView reloadData];
    [self updatePlayerCountLabel];
    [_game broadcastPlayersUpdate];
}

- (void) lobby:(GSLobby *)lobby playerDidLeave:(GSPlayer *)player {
    [_tableView reloadData];
    [self updatePlayerCountLabel];
    [_game broadcastPlayersUpdate];
}


@end
