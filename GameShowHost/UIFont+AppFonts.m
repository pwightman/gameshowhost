//
//  UIFont+AppFonts.m
//  GameShowHost
//
//  Created by Parker Wightman on 3/15/13.
//  Copyright (c) 2013 Parker Wightman. All rights reserved.
//

#import "UIFont+AppFonts.h"

@implementation UIFont (AppFonts)

+ (UIFont *)appFont {
    return [self appFontWithSize:100];
}

+ (UIFont *)appFontWithSize:(CGFloat)size {
    return [UIFont fontWithName:@"Baskerville-Bold" size:size];
}

@end
