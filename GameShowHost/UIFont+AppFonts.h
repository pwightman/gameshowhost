//
//  UIFont+AppFonts.h
//  GameShowHost
//
//  Created by Parker Wightman on 3/15/13.
//  Copyright (c) 2013 Parker Wightman. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIFont (AppFonts)

+ (UIFont *)appFont;
+ (UIFont *)appFontWithSize:(CGFloat)size;
    
@end
