//
//  GSGameOptions.h
//  GameShowHost
//
//  Created by Parker Wightman on 3/12/13.
//  Copyright (c) 2013 Parker Wightman. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "GSQuestionPack.h"

@interface GSGameOptions : NSObject

@property (strong, nonatomic) GSQuestionPack *questionPack;
@property (assign, nonatomic) BOOL autoAssignTeamNames;
@property (assign, nonatomic) NSInteger timerDuration;

@end
