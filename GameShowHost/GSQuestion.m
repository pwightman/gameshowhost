//
//  GSQuestion.m
//  GameShowHost
//
//  Created by Parker Wightman on 3/13/13.
//  Copyright (c) 2013 Parker Wightman. All rights reserved.
//

#import "GSQuestion.h"

@implementation GSQuestion

+ (id) questionWithQuestion:(NSString *)question
                     answer:(NSString *)answer
                      value:(NSInteger)value {
    
    GSQuestion *q = [[GSQuestion alloc] init];
    
    q->_question = question;
    q->_answer   = answer;
    q->_value    = value;
    
    
    return q;
}

@end
