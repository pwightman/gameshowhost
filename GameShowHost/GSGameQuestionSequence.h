//
//  GSGameQuestionSequence.h
//  GameShowHost
//
//  Created by Parker Wightman on 3/13/13.
//  Copyright (c) 2013 Parker Wightman. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "GSGame.h"
#import "GSQuestion.h"
#import "GSCategory.h"

typedef NS_ENUM(NSInteger, GSGameQuestionSequenceState) {
    GSGameQuestionSequenceStateReading,
    GSGameQuestionSequenceStateWaitingForBuzz,
    GSGameQuestionSequenceStateBuzzedIn,
    GSGameQuestionSequenceStateDoneBuzzing
};

@protocol GSGameQuestionSequenceDelegate;

@interface GSGameQuestionSequence : NSObject

@property (strong, nonatomic) GSGame                             *game;
@property (strong, nonatomic) GSCategory                         *category;
@property (strong, nonatomic) GSQuestion                         *question;

@property (weak, nonatomic) GSPlayer *playerWithBoard;

@property (assign, nonatomic) GSGameQuestionSequenceState         state;
@property (weak, nonatomic)   id<GSGameQuestionSequenceDelegate>  delegate;

- (void) startBuzzing;
- (void) endBuzzing;
- (void) playerAnswered:(BOOL)isCorrect;

+ (id)questionSequenceWithGame:(GSGame *)game
                      category:(GSCategory *)category
                      question:(GSQuestion *)question;

@end


@protocol GSGameQuestionSequenceDelegate <NSObject>

- (void) questionSequence:(GSGameQuestionSequence *)sequence shouldStartTimerWithDuration:(CGFloat)duration;
- (void) questionSequence:(GSGameQuestionSequence *)sequence playerDidBuzzEarly:(GSPlayer *)player;
- (void) questionSequence:(GSGameQuestionSequence *)sequence playerDidBuzzIn:(GSPlayer *)player;
- (void) questionSequence:(GSGameQuestionSequence *)sequence playerTimerDidRunOut:(GSPlayer *)player;
- (void) questionSequence:(GSGameQuestionSequence *)sequence questionTimerDidRunOut:(GSPlayer *)player;
- (void) questionSequence:(GSGameQuestionSequence *)sequence
    willTransitionToState:(GSGameQuestionSequenceState)toState
                fromState:(GSGameQuestionSequenceState)fromState;

@end

