//
//  UIViewController+RemoveBackButton.m
//  GameShowHost
//
//  Created by Parker Wightman on 3/14/13.
//  Copyright (c) 2013 Parker Wightman. All rights reserved.
//

#import "UIViewController+RemoveBackButton.h"

@implementation UIViewController (RemoveBackButton)

- (void) replaceBackArrowWithBarButtonItem:(UIBarButtonItem *)item {
    [self.navigationItem setHidesBackButton:YES];
    self.navigationItem.leftBarButtonItem = item;
}

@end
