//
//  GSInitialViewController.h
//  GameShowHost
//
//  Created by Parker Wightman on 3/11/13.
//  Copyright (c) 2013 Parker Wightman. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GSExternalSplashScreenViewController.h"

@interface GSInitialViewController : UIViewController

@property (weak, nonatomic) GSExternalSplashScreenViewController *externalSplashScreen;

@end
