//
//  main.m
//  GameShowHost
//
//  Created by Parker Wightman on 3/11/13.
//  Copyright (c) 2013 Parker Wightman. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "GSAppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([GSAppDelegate class]));
    }
}
