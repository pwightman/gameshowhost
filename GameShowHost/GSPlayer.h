//
//  GSPlayer.h
//  GameShowHost
//
//  Created by Parker Wightman on 3/11/13.
//  Copyright (c) 2013 Parker Wightman. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol GSPlayerNotifier;
@protocol GSPlayerDelegate;

@interface GSPlayer : NSObject

@property (strong, nonatomic) id<GSPlayerNotifier> notifier;
@property (weak, nonatomic) id<GSPlayerDelegate> delegate;

@property (strong, nonatomic) NSString *name;
@property (strong, nonatomic) NSString *identifier;
@property (assign, nonatomic) NSInteger score;

+ (id) playerWithName:(NSString *)name;

@end



@protocol GSPlayerNotifier <NSObject>

- (void) sendPlayersUpdate:(NSArray *)players;
- (void) sendGameBegan;
- (void) sendBeginBuzzing;
- (void) sendChoosingQuestion;
- (void) sendQuestionChosen;
- (void) sendWonBuzzing;
- (void) sendBuzzedEarly;

@end



@protocol GSPlayerDelegate <NSObject>

- (void) playerDidSignOut:(GSPlayer *)player;
- (void) playerDidDisconnect:(GSPlayer *)player;
- (void) playerDidBuzzIn:(GSPlayer *)player;

@end
