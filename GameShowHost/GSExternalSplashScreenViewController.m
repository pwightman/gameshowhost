//
//  GSExternalSplashScreenViewController.m
//  GameShowHost
//
//  Created by Parker Wightman on 3/14/13.
//  Copyright (c) 2013 Parker Wightman. All rights reserved.
//

#import "GSExternalSplashScreenViewController.h"

@interface GSExternalSplashScreenViewController ()
@property (weak, nonatomic) IBOutlet UILabel *splashLabel;

@end

@implementation GSExternalSplashScreenViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor blueColor];
    _splashLabel.font = [UIFont fontWithName:@"HoeflerText-Black" size:100];
    _splashLabel.numberOfLines = 3;
    [_splashLabel adjustFontSizeToFit];
	// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
