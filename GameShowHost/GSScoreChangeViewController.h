//
//  GSScoreChangeViewController.h
//  GameShowHost
//
//  Created by Parker Wightman on 3/15/13.
//  Copyright (c) 2013 Parker Wightman. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GSGame.h"

@interface GSScoreChangeViewController : UIViewController <UITableViewDataSource, UITableViewDelegate>

@property (weak, nonatomic) GSGame *game;

@property (strong, nonatomic) void (^didFinish)();

@end
