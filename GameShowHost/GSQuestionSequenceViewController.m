//
//  GSQuestionSequenceViewController.m
//  GameShowHost
//
//  Created by Parker Wightman on 3/13/13.
//  Copyright (c) 2013 Parker Wightman. All rights reserved.
//

#import "GSQuestionSequenceViewController.h"
#import "GSGameStartBuzzingView.h"
#import "GSGameEndBuzzingView.h"
#import "GSGameDoneBuzzingView.h"
#import "GSGameBuzzedInView.h"

@interface GSQuestionSequenceViewController () <GSGameQuestionSequenceDelegate>

@property (weak, nonatomic) IBOutlet UILabel *questionLabel;
@property (weak, nonatomic) IBOutlet UILabel *answerLabel;

@property (weak, nonatomic) IBOutlet UIView *questionContainer;
@property (weak, nonatomic) IBOutlet UIView *answerContainer;
@property (weak, nonatomic) IBOutlet UIView *controlsContainer;

@property (weak, nonatomic) GSPlayer *buzzedInPlayer;
@property (weak, nonatomic) IBOutlet UILabel *timerLabel;
@property (strong, nonatomic) NSTimer *timer;
@property (assign, nonatomic) CGFloat timeTimerStarted;
@property (assign, nonatomic) CGFloat timerDuration;

@end

@implementation GSQuestionSequenceViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    _questionLabel.text = _sequence.question.question;
    
    _answerLabel.text = [NSString stringWithFormat:@"Answer: %@", _sequence.question.answer];
    
    _sequence.delegate = self;
    
    [self replaceBackArrowWithBarButtonItem:nil];
}

- (void)viewWillAppear:(BOOL)animated {
    [self setUIForState:_sequence.state animated:NO];
}

- (void)setUIForState:(GSGameQuestionSequenceState)state animated:(BOOL)animated {
    UIView *newView;
    
    if ( state == GSGameQuestionSequenceStateReading ) {
        GSGameStartBuzzingView *view = [[GSGameStartBuzzingView alloc] initWithFrame:CGRectZero];
        [view.startBuzzingButton addTarget:self
                                    action:@selector(startBuzzingTapped)
                          forControlEvents:UIControlEventTouchUpInside];
        
        newView = view;
    } else if ( state == GSGameQuestionSequenceStateWaitingForBuzz ) {
        GSGameEndBuzzingView *view = [[GSGameEndBuzzingView alloc] initWithFrame:CGRectZero];
        
        
        [view.endBuzzingButton addTarget:self
                                  action:@selector(endBuzzingTapped)
                        forControlEvents:UIControlEventTouchUpInside];
        
        newView = view;
    } else if ( state == GSGameQuestionSequenceStateBuzzedIn ) {
        GSGameBuzzedInView *view = [[GSGameBuzzedInView alloc] initWithFrame:CGRectZero];
        
        [view.correctButton addTarget:self action:@selector(correctTapped) forControlEvents:UIControlEventTouchUpInside];
        [view.incorrectButton addTarget:self action:@selector(incorrectTapped) forControlEvents:UIControlEventTouchUpInside];
//        [view.correctButton touchUpInside:^(UIEvent *event) {
//        }];
//        
//        [view.incorrectButton touchUpInside:^(UIEvent *event) {
//        }];
        
        view.buzzedInPlayerLabel.text = _buzzedInPlayer.name;
        
        newView = view;
    } else if ( state == GSGameQuestionSequenceStateDoneBuzzing ) {
        GSGameDoneBuzzingView *view = [[GSGameDoneBuzzingView alloc] initWithFrame:CGRectZero];
        
        [view.continueButton addTarget:self action:@selector(continueTapped) forControlEvents:UIControlEventTouchUpInside];
        
        newView = view;
    }
    
    CGFloat duration = (animated ? 0.3 : 0.0);
    
    [self animateInNewView:newView withDuration:duration];
    
}

- (void) startBuzzingTapped {
    [_sequence startBuzzing];
}

- (void) endBuzzingTapped {
    [_sequence endBuzzing];
}

- (void) correctTapped {
    NSLog(@"Correct button tapped");
    [_sequence playerAnswered:YES];
}

- (void) incorrectTapped {
    NSLog(@"Incorrect button tapped");
    [_sequence playerAnswered:NO];
}

- (void) continueTapped {
    if ( _didFinishWithPlayerHavingBoard ) {
        _didFinishWithPlayerHavingBoard(_sequence.playerWithBoard);
    }
}

- (void) animateInNewView:(UIView *)newView withDuration:(CGFloat)duration {
    __block UIView *previousView = nil;
    
    [[_controlsContainer subviews] enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        previousView = obj;
    }];
    
    CGRect originalFrame = _controlsContainer.bounds;
    CGRect newFrame = _controlsContainer.bounds;
    newFrame.origin.x += newFrame.size.width;
    
    CGRect newViewFrame = _controlsContainer.bounds;
    newViewFrame.origin.x -= _controlsContainer.frame.size.width;
    newView.frame = newViewFrame;
    newViewFrame.origin.x += _controlsContainer.frame.size.width;
    
    [_controlsContainer addSubview:newView];
    
    [UIView animateWithDuration:duration
                     animations:^{
                         previousView.frame = newFrame;
                         newView.frame = originalFrame;
                     } completion:^(BOOL finished) {
                         [previousView removeFromSuperview];
                     }];
    
}

- (void) questionSequence:(GSGameQuestionSequence *)sequence playerDidBuzzIn:(GSPlayer *)player {
    _buzzedInPlayer = player;
    [player.notifier sendWonBuzzing];
}

- (void) questionSequence:(GSGameQuestionSequence *)sequence playerDidBuzzEarly:(GSPlayer *)player {
    [player.notifier sendBuzzedEarly];
}

- (void) questionSequence:(GSGameQuestionSequence *)sequence playerTimerDidRunOut:(GSPlayer *)player {
    
}

- (void) questionSequence:(GSGameQuestionSequence *)sequence questionTimerDidRunOut:(GSPlayer *)player {
}

- (void) timerDidFire {
    CGFloat currentTime = CACurrentMediaTime();
    CGFloat elapsed = currentTime - _timeTimerStarted;
    
    if ( elapsed < _timerDuration ) {
        _timerLabel.text = [NSString stringWithFormat:@"%.1f", _timerDuration - elapsed];
    }
}

- (void) questionSequence:(GSGameQuestionSequence *)sequence shouldStartTimerWithDuration:(CGFloat)duration {
    if (_timer) {
        [_timer invalidate];
    }
    
    _timeTimerStarted = CACurrentMediaTime();
    _timerDuration = duration;
    
    _timer = [NSTimer scheduledTimerWithTimeInterval:0.01
                                              target:self
                                            selector:@selector(timerDidFire)
                                            userInfo:nil
                                             repeats:YES];
}

- (void) questionSequence:(GSGameQuestionSequence *)sequence
    willTransitionToState:(GSGameQuestionSequenceState)toState
                fromState:(GSGameQuestionSequenceState)fromState {
    [self setUIForState:toState animated:YES];
    
    if ( toState == GSGameQuestionSequenceStateWaitingForBuzz )
        [_externalInGameController buzzingBegan];
    else if ( fromState == GSGameQuestionSequenceStateWaitingForBuzz ) {
        [_externalInGameController buzzingEnded];
    }
    
    if ( toState == GSGameQuestionSequenceStateDoneBuzzing ) {
        [_timer invalidate];
    }
}
@end
