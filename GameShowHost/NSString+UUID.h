//
//  NSString+UUID.h
//  GameShowHost
//
//  Created by Parker Wightman on 3/12/13.
//  Copyright (c) 2013 Parker Wightman. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (UUID)

+ (NSString *)UUID;

@end
