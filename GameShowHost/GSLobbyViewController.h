//
//  GSLobbyViewController.h
//  GameShowHost
//
//  Created by Parker Wightman on 3/11/13.
//  Copyright (c) 2013 Parker Wightman. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GSExternalLobbyViewController.h"

@class GSGameOptions;

@interface GSLobbyViewController : UIViewController

@property (strong, nonatomic) GSGameOptions *gameOptions;

@property (weak, nonatomic) GSExternalLobbyViewController *externalLobbyController;

@property (strong, nonatomic) void (^didFinish)();

@end
