//
//  GSGameBuzzedInView.m
//  GameShowHost
//
//  Created by Parker Wightman on 3/13/13.
//  Copyright (c) 2013 Parker Wightman. All rights reserved.
//

#import "GSGameBuzzedInView.h"

@implementation GSGameBuzzedInView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        _buzzedInPlayerLabel = [[UILabel alloc] initWithFrame:CGRectZero];
        _correctButton = [UIButton buttonWithType:UIButtonTypeCustom];
        _incorrectButton = [UIButton buttonWithType:UIButtonTypeCustom];
        
        [_correctButton setTitle:@"Correct" forState:UIControlStateNormal];
        [_incorrectButton setTitle:@"Incorrect" forState:UIControlStateNormal];
        
        [_correctButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        _correctButton.titleLabel.shadowColor = [UIColor blackColor];
        _correctButton.titleLabel.shadowOffset = CGSizeMake(1, 1);
        _correctButton.titleLabel.font = [UIFont appFontWithSize:50];
        _correctButton.backgroundColor = [UIColor greenColor];
        
        [_incorrectButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        _incorrectButton.titleLabel.shadowColor = [UIColor blackColor];
        _incorrectButton.titleLabel.shadowOffset = CGSizeMake(1, 1);
        _incorrectButton.titleLabel.font = [UIFont appFontWithSize:50];
        _incorrectButton.backgroundColor = [UIColor redColor];
        
        [self addSubview:_buzzedInPlayerLabel];
        [self addSubview:_correctButton];
        [self addSubview:_incorrectButton];
    }
    return self;
}

- (void) layoutSubviews {
    CGFloat width = self.frame.size.width / 3;
    CGFloat height = self.frame.size.height;
    
    _buzzedInPlayerLabel.frame = CGRectMake(width * 0, 0, width, height);
    _incorrectButton.frame     = CGRectMake(width * 1, 0, width, height);
    _correctButton.frame       = CGRectMake(width * 2, 0, width, height);
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
