//
//  GSAppDelegate.m
//  GameShowHost
//
//  Created by Parker Wightman on 3/11/13.
//  Copyright (c) 2013 Parker Wightman. All rights reserved.
//

#import "GSAppDelegate.h"
#import "GSExternalSplashScreenViewController.h"
#import "GSInitialViewController.h"

@interface GSAppDelegate ()

@property (strong, nonatomic) UIWindow *externalWindow;

@end

@implementation GSAppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    UINavigationController *navController = (UINavigationController *)[_window rootViewController];
    GSInitialViewController *initialController = [[navController viewControllers] objectAtIndex:0];
    
    
    [[UIApplication sharedApplication] setIdleTimerDisabled:YES];
    
    _externalWindow = [[UIWindow alloc] initWithFrame:CGRectZero];
    GSExternalSplashScreenViewController *splashController = [initialController.storyboard instantiateViewControllerWithIdentifier:STORYBOARD_ID_EXTERNAL_SPLASH];
    
    initialController.externalSplashScreen = splashController;
    
    navController = [[UINavigationController alloc] initWithRootViewController:splashController];
    navController.navigationBarHidden = YES;
    
    _externalWindow.rootViewController = navController;
    
    [self attachExternalScreen];
    
    NSNotificationCenter *center = [NSNotificationCenter defaultCenter];
    
    [center addObserver:self
               selector:@selector(handleScreenDidConnect:)
                   name:UIScreenDidConnectNotification
                 object:nil];
    
    [center addObserver:self
               selector:@selector(handleScreenDidDisconnect:)
                   name:UIScreenDidDisconnectNotification
                 object:nil];
    
    return YES;
}

- (void) handleScreenDidConnect:(NSNotification *)aNotification {
    [self attachExternalScreen];
}

- (void) handleScreenDidDisconnect:(NSNotification *)aNotification {
    
}

- (void) attachExternalScreen {
    if ( [[UIScreen screens] count] > 1 ) {
        UIScreen *secondScreen = [[UIScreen screens] objectAtIndex:1];
        
        secondScreen.overscanCompensation = UIScreenOverscanCompensationScale;
        
        CGRect screenBounds = secondScreen.bounds;
        
        _externalWindow.frame = screenBounds;
        _externalWindow.screen = secondScreen;
        
        
        [_externalWindow setNeedsLayout];
        _externalWindow.hidden = NO;
    }
}
							
- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

@end
