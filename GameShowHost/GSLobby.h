//
//  GSLobby.h
//  GameShowHost
//
//  Created by Parker Wightman on 3/11/13.
//  Copyright (c) 2013 Parker Wightman. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol GSLobbyDelegate;

@class GSPlayer;
@class GSGameOptions;

@interface GSLobby : NSObject

@property (weak, nonatomic) id<GSLobbyDelegate> delegate;
@property (weak, nonatomic, readonly) NSArray *players;
@property (strong, nonatomic) GSGameOptions *gameOptions;

- (void) start;
- (void) stop;

@end

@protocol GSLobbyDelegate <NSObject>

- (void) lobby:(GSLobby *)lobby failedToStartServer:(NSError *)error;
- (void) lobbyDidStartServer:(GSLobby *)lobby;
- (void) lobby:(GSLobby *)lobby playerDidJoin:(GSPlayer *)player;
- (void) lobby:(GSLobby *)lobby playerDidLeave:(GSPlayer *)player;

@end

