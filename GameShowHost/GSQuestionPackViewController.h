//
//  GSQuestionPackViewController.h
//  GameShowHost
//
//  Created by Parker Wightman on 3/18/13.
//  Copyright (c) 2013 Parker Wightman. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GSQuestionPack.h"

@interface GSQuestionPackViewController : UIViewController <UITableViewDataSource, UITableViewDelegate>

@property (strong, nonatomic) void (^didFinishWithQuestionPack)(GSQuestionPack *questionPack);

@end
