//
//  GSGameOptionsViewController.m
//  GameShowHost
//
//  Created by Parker Wightman on 3/12/13.
//  Copyright (c) 2013 Parker Wightman. All rights reserved.
//

#import "GSGameOptionsViewController.h"
#import "GSLobbyViewController.h"
#import "GSGameOptions.h"
#import "GSQuestionPackViewController.h"
#import "GSExternalLobbyViewController.h"

@interface GSGameOptionsViewController ()

@property (strong, nonatomic) GSGameOptions *gameOptions;

@property (weak, nonatomic) IBOutlet UISwitch *autoAssignSwitch;
@property (weak, nonatomic) IBOutlet UISlider *durationSlider;
@property (weak, nonatomic) IBOutlet UILabel *durationLabel;

@end

@implementation GSGameOptionsViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    _gameOptions = [[GSGameOptions alloc] init];
    [self updateControlValuesInGameOptions];
	// Do any additional setup after loading the view.
}

- (void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ( [segue.identifier isEqualToString:@"lobby"] ) {
        GSLobbyViewController *viewController = (GSLobbyViewController *)segue.destinationViewController;
        
        if (_gameOptions.questionPack == nil )
            _gameOptions.questionPack = [GSQuestionPack questionPackFromContentsOfFileAtPath:[[NSBundle mainBundle] pathForResource:@"wightman_1" ofType:@"json"]];
        
        GSExternalLobbyViewController *lobbyController = [self.storyboard instantiateViewControllerWithIdentifier:STORYBOARD_ID_EXTERNAL_LOBBY];
        
        [_externalSplashScreen.navigationController pushViewController:lobbyController animated:YES];
        
        viewController.gameOptions = _gameOptions;
        viewController.externalLobbyController = lobbyController;
        viewController.didFinish = ^{
            [self.navigationController popViewControllerAnimated:YES];
            [_externalSplashScreen.navigationController popViewControllerAnimated:YES];
        };
    } else if ( [segue.identifier isEqualToString:@"chooseQuestionPack"] ) {
        UIStoryboardPopoverSegue *popoverSegue = (UIStoryboardPopoverSegue *)segue;
        GSQuestionPackViewController* controller = (GSQuestionPackViewController *)[((UINavigationController *)segue.destinationViewController) topViewController];
        
        controller.didFinishWithQuestionPack = ^(GSQuestionPack *questionPack) {
            _gameOptions.questionPack = questionPack;
            [popoverSegue.popoverController dismissPopoverAnimated:YES];
        };
    }
}

- (void) updateControlValuesInGameOptions {
    _gameOptions.autoAssignTeamNames = [_autoAssignSwitch isOn];
    _gameOptions.timerDuration = (NSInteger)[_durationSlider value];
}

- (IBAction)autoAssignTapped:(id)sender {
    [self updateControlValuesInGameOptions];
}

- (IBAction)durationSliderChanged:(id)sender {
    _durationLabel.text = [NSString stringWithFormat:@"%@", @((NSInteger)_durationSlider.value)];
    [self updateControlValuesInGameOptions];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
