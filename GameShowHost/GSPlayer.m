//
//  GSPlayer.m
//  GameShowHost
//
//  Created by Parker Wightman on 3/11/13.
//  Copyright (c) 2013 Parker Wightman. All rights reserved.
//

#import "GSPlayer.h"


@implementation GSPlayer

+ (id) playerWithName:(NSString *)name {
    GSPlayer *player = [[self alloc] init];
    player.name = name;
    player.identifier = [NSString UUID];
    player.score = 0;
    
    return player;
}

@end
