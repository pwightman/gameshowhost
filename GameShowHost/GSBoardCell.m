//
//  GSBoardCell.m
//  GameShowHost
//
//  Created by Parker Wightman on 3/13/13.
//  Copyright (c) 2013 Parker Wightman. All rights reserved.
//

#import "GSBoardCell.h"
#import <QuartzCore/QuartzCore.h>

@implementation GSBoardCell

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor clearColor];
        
        _label = [[UILabel alloc] initWithFrame:CGRectZero];
        _label.font = [UIFont appFont];
        _label.minimumScaleFactor = 0.1;
        _label.numberOfLines = 5;
        _label.adjustsFontSizeToFitWidth = YES;
        _label.textColor = [UIColor whiteColor];
        _label.textAlignment = NSTextAlignmentCenter;
        
        _label.shadowColor = [UIColor blackColor];
        _label.shadowOffset = CGSizeMake(1, 1);
        
        
        _label.backgroundColor = [UIColor clearColor];
        self.layer.borderWidth = 1;
        self.layer.borderColor = [[UIColor colorWithRed:0.2 green:0.2 blue:0.2 alpha:1.0] CGColor];
        
        [self addSubview:_label];
    }
    return self;
}

- (void) layoutSubviews {
    CGRect frame = self.bounds;
    frame.size.width *= 0.75;
    frame.size.height *= 0.75;
    frame.origin.x = (0.25 * self.frame.size.width) / 2;
    frame.origin.y = (0.25 * self.frame.size.height) / 2;
    _label.frame = CGRectInset(CGRectIntegral(frame), 10, 10);
    [_label adjustFontSizeToFit];
}

@end
