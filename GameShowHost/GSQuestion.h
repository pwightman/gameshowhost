//
//  GSQuestion.h
//  GameShowHost
//
//  Created by Parker Wightman on 3/13/13.
//  Copyright (c) 2013 Parker Wightman. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface GSQuestion : NSObject

@property (strong, nonatomic, readonly) NSString  *question;
@property (strong, nonatomic, readonly) NSString  *answer;
@property (assign, nonatomic, readonly) NSInteger  value;

+ (id) questionWithQuestion:(NSString *)question
                     answer:(NSString *)answer
                      value:(NSInteger)value;

@end
