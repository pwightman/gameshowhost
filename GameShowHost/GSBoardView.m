//
//  GSBoardView.m
//  GameShowHost
//
//  Created by Parker Wightman on 3/13/13.
//  Copyright (c) 2013 Parker Wightman. All rights reserved.
//

#import "GSBoardView.h"
#import "GSBoardCell.h"

@interface GSBoardView ()

@property (strong, nonatomic) NSMutableArray *cells;

@end

@implementation GSBoardView

- (id) initWithBoard:(GSBoard *)board {
    self = [super initWithFrame:CGRectZero];
    
    if ( self ) {
        self.backgroundColor = [UIColor blueColor];
        _board = board;
        _cells = [NSMutableArray array];
        [self fillCells];
        [self catchEvents];
    }
    
    return self;
}

- (void) fillCells {
    for (UIView *view in self.subviews) {
        [view removeFromSuperview];
    }
    
    _cells = [NSMutableArray array];
    
    for (NSInteger i = 0; i < _board.questionPack.categoriesCount; i++) {
        NSMutableArray *category = [NSMutableArray array];
        GSBoardCell *categoryCell = [[GSBoardCell alloc] initWithFrame:CGRectZero];
        
        categoryCell.label.text = [[_board.questionPack.categories objectAtIndex:i] name];
        categoryCell.column = i;
        categoryCell.row = 0;
        
        [category addObject:categoryCell];
        [self addSubview:categoryCell];
        
        for (NSInteger j = 0; j < _board.questionPack.questionsPerCategoryCount; j++) {
            GSBoardCell *questionCell = [[GSBoardCell alloc] initWithFrame:CGRectZero];
            
        if (![[[_board.playedQuestions objectAtIndex:i] objectAtIndex:j] boolValue]) {
            questionCell.label.text = [NSString stringWithFormat:@"%@", @(((GSQuestion *)[[_board.questionPack.questions objectAtIndex:i] objectAtIndex:j]).value)];
        }
            
            questionCell.column = i;
            questionCell.row = j;
            
            [category addObject:questionCell];
            [self addSubview:questionCell];
        }
        
        [_cells addObject:category];
    }
}

- (void) catchEvents {
    for (NSInteger i = 0; i < _cells.count; i++) {
        NSArray *category = [_cells objectAtIndex:i];
        for (NSInteger j = 1; j < category.count; j++) {
            GSBoardCell *cell = [category objectAtIndex:j];
            
            [cell removeBlocksForControlEvents:UIControlEventAllTouchEvents];
            
            // We don't want to catch events for squares that have already been played.
            if (![[[_board.playedQuestions objectAtIndex:i] objectAtIndex:j - 1] boolValue]) {
                [cell touchUpInside:^(UIEvent *event) {
                    [_delegate boardView:self questionSelectedAtCategory:i question:j - 1];
                }];
            }
        }
    }
}

- (void) layoutSubviews {
    [self fillCells];
    [self catchEvents];
    for (NSInteger i = 0; i < _cells.count; i++) {
        NSArray *category = [_cells objectAtIndex:i];
        for (NSInteger j = 0; j < category.count; j++) {
            GSBoardCell *cell = [[_cells objectAtIndex:i] objectAtIndex:j];
            CGFloat width  = (self.frame.size.width / _cells.count);
            CGFloat height = (self.frame.size.height / category.count);
            CGFloat x      = (width  * i);
            CGFloat y      = (height * j);
            
            cell.frame = CGRectIntegral(CGRectMake(x, y, width, height));
        }
    }
}

@end
