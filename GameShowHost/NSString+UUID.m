//
//  NSString+UUID.m
//  GameShowHost
//
//  Created by Parker Wightman on 3/12/13.
//  Copyright (c) 2013 Parker Wightman. All rights reserved.
//

#import "NSString+UUID.h"

@implementation NSString (UUID)

+ (NSString *)UUID {
    return [[NSUUID UUID] UUIDString];
}

@end
