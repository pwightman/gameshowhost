//
//  GSChooseQuestionViewController.m
//  GameShowHost
//
//  Created by Parker Wightman on 3/13/13.
//  Copyright (c) 2013 Parker Wightman. All rights reserved.
//

#import "GSChooseQuestionViewController.h"
#import "GSGameQuestionSequence.h"
#import "GSQuestionSequenceViewController.h"
#import "GSScoreChangeViewController.h"
#import "GSGameQuestionSequence.h"
#import "GSBoardView.h"
#import "GSQuestionPackViewController.h"

@interface GSChooseQuestionViewController () <GSBoardViewDelegate>

@property (weak, nonatomic) IBOutlet UIView *boardContainer;
@property (weak, nonatomic) IBOutlet UIView *toolbarContainer;
@property (strong, nonatomic) GSBoardView     *boardView;

@property (weak, nonatomic) GSPlayer *playerWithBoard;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *optionsBarButtonItem;

@end

@implementation GSChooseQuestionViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    _boardView = [[GSBoardView alloc] initWithBoard:_game.board];
    
    _boardView.frame = _boardContainer.bounds;
    _boardView.delegate = self;
    [_boardContainer addSubview:_boardView];
    
    for (GSPlayer *player in _game.players) {
        [player.notifier sendGameBegan];
    }
    
    [self replaceBackArrowWithBarButtonItem:nil];
    
    if ( _game.players.count > 0 )
        _playerWithBoard = [_game.players objectAtIndex:0];
}

- (void)viewWillAppear:(BOOL)animated {
    [self.boardView setNeedsLayout];
}

- (void) boardView:(GSBoardView *)boardView questionSelectedAtCategory:(NSInteger)category question:(NSInteger)question {
    
    [[_game.board.playedQuestions objectAtIndex:category] setObject:@(YES) atIndex:question];
    
    NSLog(@"Question selected at %@, %@", @(category), @(question));
    GSQuestion *q = [[_game.board.questionPack.questions objectAtIndex:category] objectAtIndex:question];
    GSCategory *cat = [_game.board.questionPack.categories objectAtIndex:category];
    
    GSQuestionSequenceViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"questionSequence"];
    
    GSGameQuestionSequence *sequence = [GSGameQuestionSequence questionSequenceWithGame:_game
                                                                               category:cat
                                                                               question:q];
    
    sequence.playerWithBoard = _playerWithBoard;
    
    controller.sequence = sequence;
    controller.externalInGameController = _externalInGameController;
    
    [_externalInGameController selectQuestion:question inCategory:category];
    
    controller.didFinishWithPlayerHavingBoard = ^(GSPlayer *player ) {
        _playerWithBoard = player;
        [self.navigationController popViewControllerAnimated:YES];
        [_externalInGameController finishQuestion];
    };
    
    [_boardView setNeedsLayout];
    
    [self.navigationController pushViewController:controller animated:YES];
}

- (IBAction)optionsTapped:(id)sender {
    PSPDFActionSheet *actionSheet = [[PSPDFActionSheet alloc] initWithTitle:@"Options"];
    
    [actionSheet addButtonWithTitle:@"Quit Game"
                              block:^{
                                  if (_didFinish)
                                      _didFinish();
                              }];
    
    [actionSheet addButtonWithTitle:@"See Scores"
                              block:^{
                                  UINavigationController *navController = [self.storyboard instantiateViewControllerWithIdentifier:@"scoreChangeNav"];
                                  GSScoreChangeViewController *controller = (GSScoreChangeViewController *)[navController topViewController];
                                  controller.game = _game;
                                  navController.modalPresentationStyle = UIModalPresentationFormSheet;
                                  navController.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
                                  controller.didFinish = ^{
                                      [self dismissViewControllerAnimated:YES completion:^{}];
                                  };
                                  
                                  [self presentViewController:navController animated:YES completion:^{}];
                              }];
    
    [actionSheet addButtonWithTitle:@"Load New Round"
                              block:^{
                                  [self presentLoadNewRoundController];
                              }];
    
    [actionSheet showFromBarButtonItem:_optionsBarButtonItem animated:YES];
}

- (void) presentLoadNewRoundController {
    UINavigationController *navController = [self.storyboard instantiateViewControllerWithIdentifier:@"navQuestionPack"];
    GSQuestionPackViewController *controller = (GSQuestionPackViewController *)navController.topViewController;
    
    controller.didFinishWithQuestionPack = ^(GSQuestionPack *questionPack) {
        NSLog(@"%@", questionPack);
        [self loadNewRoundWithQuestionPack:questionPack];
        [self dismissViewControllerAnimated:YES
                                 completion:^{ }];
    };
    
    navController.modalPresentationStyle = UIModalPresentationFormSheet;
    navController.modalTransitionStyle = UIModalTransitionStyleCoverVertical;
    
    [self presentViewController:navController
                       animated:YES
                     completion:^{ }];
}

- (void) loadNewRoundWithQuestionPack:(GSQuestionPack *)questionPack {
    _game.board = [GSBoard boardWithQuestionPack:questionPack];
    _boardView.board = _game.board;
    [_externalInGameController loadNewBoard:_game.board];
    [_boardView setNeedsLayout];
}

@end
