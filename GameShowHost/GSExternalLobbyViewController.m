//
//  GSExternalLobbyViewController.m
//  GameShowHost
//
//  Created by Parker Wightman on 3/14/13.
//  Copyright (c) 2013 Parker Wightman. All rights reserved.
//

#import "GSExternalLobbyViewController.h"

@interface GSExternalLobbyViewController ()
@property (weak, nonatomic) IBOutlet UILabel *waitingLabel;

@end

@implementation GSExternalLobbyViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor blueColor];
    _waitingLabel.font = [UIFont appFont];
    [_waitingLabel adjustFontSizeToFit];
	// Do any additional setup after loading the view.
}

@end
