//
//  UILabel+AdjustFontSize.h
//  GameShowHost
//
//  Created by Parker Wightman on 3/15/13.
//  Copyright (c) 2013 Parker Wightman. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UILabel (AdjustFontSize)

- (void)adjustFontSizeToFit;

@end
