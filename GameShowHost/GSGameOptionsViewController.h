//
//  GSGameOptionsViewController.h
//  GameShowHost
//
//  Created by Parker Wightman on 3/12/13.
//  Copyright (c) 2013 Parker Wightman. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GSExternalSplashScreenViewController.h"

@interface GSGameOptionsViewController : UIViewController

@property (weak, nonatomic) GSExternalSplashScreenViewController *externalSplashScreen;

@end
