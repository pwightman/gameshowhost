//
//  GSLobby.m
//  GameShowHost
//
//  Created by Parker Wightman on 3/11/13.
//  Copyright (c) 2013 Parker Wightman. All rights reserved.
//

#import "GSLobby.h"
#import "GSPlayer.h"
#import <DTBonjour/DTBonjourServer.h>
#import "GSProtocol.h"
#import "GSPlayerNetworkNotifier.h"
#import "GSGameOptions.h"

#define GSGameBonjourType @"_ComAloraStudiosGameShow._tcp."

@interface GSLobby () <DTBonjourServerDelegate>

@property (strong, nonatomic) NSMutableArray *mutPlayers;
@property (strong, nonatomic) NSMutableDictionary *potenialPlayers; // Data Connections that haven't quite finished the login process
@property (strong, nonatomic) DTBonjourServer *server;

@end

@implementation GSLobby

- (id) init {
    self = [super init];
    
    if ( self ) {
        _mutPlayers = [@[] mutableCopy];
        _players = _mutPlayers;
    }
    
    return self;
}

- (void) start {
    _server = [[DTBonjourServer alloc] initWithBonjourType:GSGameBonjourType];
    _server.delegate = self;
    
    if ( [_server start] ) {
        [_delegate lobbyDidStartServer:self];
    } else {
        [_delegate lobby:self failedToStartServer:[NSError errorWithDomain:@"It didn't start." code:0 userInfo:@{ @"cause": @"it sucks" }]];
    }
}

- (void) stop {
    [_server stop];
}

- (void) bonjourServer:(DTBonjourServer *)server didAcceptConnection:(DTBonjourDataConnection *)connection {
    NSLog(@"Accepted Connection.");
    
    connection.sendingContentType = DTBonjourDataConnectionContentTypeJSON;
}

- (void) bonjourServer:(DTBonjourServer *)server didReceiveObject:(id)object onConnection:(DTBonjourDataConnection *)connection {
    GSProtocol protocol = [[object objectForKey:@"protocol"] integerValue];
    
    NSLog(@"Received Protocol: %@", @(protocol));
    
    if ( protocol == GSProtocolSignInRequest ) {
        
        NSError *error = nil;
        if (_gameOptions.autoAssignTeamNames) {
            [self loginUserWithName:@"justin" connection:connection];
        } else {
            [connection sendObject:@{ @"protocol": @(GSProtocolTeamNameRequest) } error:&error];
            NSLog(@"Requesting team name from new user.");
            
            NSAssert(error == nil, @"Error sending GSProtocolSignIn to player: %@", error);
        }
    } else if ( protocol == GSProtocolTeamNameRequest ) {
        
        [self loginUserWithName:[object objectForKey:@"name"] connection:connection];
    } else if ( protocol == GSProtocolBuzzIn ) {
        
        NSLog(@"Buzz!");
    }
}

- (void) loginUserWithName:(NSString *)name connection:(DTBonjourDataConnection *)connection {
        NSLog(@"User %@ is logged in.", name);
        
        NSAssert(name != nil, @"Error: name attribute wasn't sent with protocol GSProtocolTeamNameRequest.");
        
        GSPlayer *player = [GSPlayer playerWithName:name];
        GSPlayerNetworkNotifier *notifier = [GSPlayerNetworkNotifier notifierWithConnection:connection];
        
        notifier.player = player;
        player.notifier = notifier;
        
        
        [_mutPlayers addObject:player];
        
        NSError *error = nil;
        [connection sendObject:@{ @"protocol": @(GSProtocolSignedIn), @"name": name } error:&error];
        
        if ( error ) {
            NSLog(@"Error trying to send GSProtocolSignedIn in GSLobby.m");
        }
        
        [_delegate lobby:self playerDidJoin:player];
    
}

- (void) broadcastPlayersUpdate {
    
}

@end
