//
//  GSChooseQuestionViewController.h
//  GameShowHost
//
//  Created by Parker Wightman on 3/13/13.
//  Copyright (c) 2013 Parker Wightman. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GSGame.h"
#import "GSExternalInGameViewController.h"

@interface GSChooseQuestionViewController : UIViewController

@property (strong, nonatomic) GSGame *game;

@property (weak, nonatomic) GSExternalInGameViewController *externalInGameController;

@property (strong, nonatomic) void (^didFinish)();

@end
