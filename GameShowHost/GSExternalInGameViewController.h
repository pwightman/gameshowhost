//
//  GSExternalInGameViewController.h
//  GameShowHost
//
//  Created by Parker Wightman on 3/14/13.
//  Copyright (c) 2013 Parker Wightman. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GSBoard.h"
#import "GSPlayer.h"
#import "GSBoardView.h"

@interface GSExternalInGameViewController : UIViewController

@property (weak, nonatomic) GSBoard *board;

- (void) loadNewBoard:(GSBoard *)board;

- (void) selectQuestion:(NSInteger)question inCategory:(NSInteger)category;
- (void) buzzingBegan;
- (void) buzzingEnded;
- (void) playerBuzzedIn:(GSPlayer *)player;
- (void) playerAnswered:(BOOL)isCorrect;
- (void) showAnswer;
- (void) finishQuestion;
- (void) showScores:(NSArray *)players;

@end
