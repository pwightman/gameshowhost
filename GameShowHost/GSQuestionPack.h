//
//  GSQuestionPack.h
//  GameShowHost
//
//  Created by Parker Wightman on 3/13/13.
//  Copyright (c) 2013 Parker Wightman. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "GSQuestion.h"
#import "GSCategory.h"

@interface GSQuestionPack : NSObject

@property (strong, nonatomic) NSArray   *categories;
@property (strong, nonatomic) NSArray   *questions;
@property (strong, nonatomic) NSString  *formatVersion;
@property (strong, nonatomic) NSString  *name;
@property (strong, nonatomic) NSString  *dateUpdated;

// These can be inferred but they're also nice to have.
@property (assign, nonatomic) NSInteger categoriesCount;
@property (assign, nonatomic) NSInteger questionsPerCategoryCount;

+ (id)questionPackFromJSON:(NSDictionary *)dictionary;
+ (id)questionPackFromContentsOfFileAtPath:(NSString *)path;
- (NSDictionary *)toJSON;

@end
