//
//  GSQuestionPack.m
//  GameShowHost
//
//  Created by Parker Wightman on 3/13/13.
//  Copyright (c) 2013 Parker Wightman. All rights reserved.
//

#import "GSQuestionPack.h"

@implementation GSQuestionPack

+ (id)questionPackFromContentsOfFileAtPath:(NSString *)path {
    NSData *data = [NSData dataWithContentsOfFile:path];
    NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
    
    return [self questionPackFromJSON:dict];
}

+ (id) questionPackFromJSON:(NSDictionary *)dictionary {
    GSQuestionPack *questionPack = [[GSQuestionPack alloc] init];
    
    questionPack.name = [dictionary objectForKey:@"name"];
    questionPack.formatVersion = [dictionary objectForKey:@"format_version"];
    questionPack.categoriesCount = [[dictionary objectForKey:@"categories_count"] integerValue];
    questionPack.questionsPerCategoryCount = [[dictionary objectForKey:@"questions_per_category_count"] integerValue];
    questionPack.dateUpdated = [dictionary objectForKey:@"updated_at"];
    
    NSMutableArray *categories = [NSMutableArray arrayWithCapacity:questionPack.categoriesCount];
    NSMutableArray *questionArrays = [NSMutableArray arrayWithCapacity:questionPack.categoriesCount];
    
    for (NSInteger i = 0; i < [[dictionary objectForKey:@"categories_count"] integerValue]; i++) {
        GSCategory *category = [GSCategory categoryWithName:[[[dictionary objectForKey:@"categories"] objectAtIndex:i] objectForKey:@"category"]];
        
        [categories addObject:category];
        
        NSMutableArray *questions = [NSMutableArray arrayWithCapacity:questionPack.questionsPerCategoryCount];
        
        for (NSInteger j = 0; j < questionPack.questionsPerCategoryCount; j++) {
            NSDictionary *jsonQuestion = [[[[dictionary objectForKey:@"categories"] objectAtIndex:i] objectForKey:@"questions"] objectAtIndex:j];
            GSQuestion *question = [GSQuestion questionWithQuestion:[jsonQuestion objectForKey:@"question"]
                                                             answer:[jsonQuestion objectForKey:@"answer"]
                                                              value:[[jsonQuestion objectForKey:@"value"] integerValue]];
            [questions addObject:question];
        }
        
        [questionArrays addObject:questions];
        
    }
    
    questionPack.categories = categories;
    questionPack.questions = questionArrays;
    
    return questionPack;
}

- (NSDictionary *)toJSON {
    NSMutableDictionary *dict = [NSMutableDictionary dictionary];
    
    [dict setObject:_name forKey:@"name"];
    [dict setObject:_formatVersion forKey:@"format_version"];
    [dict setObject:@(_categoriesCount) forKey:@"categories_count"];
    [dict setObject:@(_questionsPerCategoryCount) forKey:@"questions_per_category_count"];
    [dict setObject:_dateUpdated forKey:@"updated_at"];
    
    NSMutableArray *categories = [NSMutableArray array];
    
    for (NSInteger i = 0; i < _categoriesCount; i++) {
        NSMutableDictionary *category = [NSMutableDictionary dictionary];
        GSCategory *nativeCategory = [_categories objectAtIndex:i];
        [category setObject:nativeCategory.name forKey:@"category"];
        
        NSMutableArray *questions = [NSMutableArray array];
        
        for (NSInteger j = 0; j < _questionsPerCategoryCount; j++) {
            NSMutableDictionary *question = [NSMutableDictionary dictionary];
            GSQuestion *nativeQuestion = [[_questions objectAtIndex:i] objectAtIndex:j];
            [question setObject:nativeQuestion.question forKey:@"question"];
            [question setObject:nativeQuestion.answer forKey:@"answer"];
            [question setObject:@(nativeQuestion.value) forKey:@"value"];
            [questions addObject:question];
        }
        
        [category setObject:questions forKey:@"questions"];
        [categories addObject:category];
    }
    
    [dict setObject:categories forKey:@"categories"];
    
    return dict;
    
}

@end
