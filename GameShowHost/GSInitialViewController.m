//
//  GSInitialViewController.m
//  GameShowHost
//
//  Created by Parker Wightman on 3/11/13.
//  Copyright (c) 2013 Parker Wightman. All rights reserved.
//

#import "GSInitialViewController.h"
#import <MTBlockTableView/MTBlockTableView.h>
#import "GSGameOptionsViewController.h"

@interface GSInitialViewController ()

@end

@implementation GSInitialViewController

- (void)viewDidLoad
{

}

- (void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ( [segue.identifier isEqualToString:@"gameOptions"] ) {
        GSGameOptionsViewController *controller = (GSGameOptionsViewController *)segue.destinationViewController;
        
        controller.externalSplashScreen = _externalSplashScreen;
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
