//
//  GSGameStartBuzzingView.m
//  GameShowHost
//
//  Created by Parker Wightman on 3/13/13.
//  Copyright (c) 2013 Parker Wightman. All rights reserved.
//

#import "GSGameStartBuzzingView.h"

@implementation GSGameStartBuzzingView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        _startBuzzingButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
        [_startBuzzingButton setTitle:@"Start Buzzing" forState:UIControlStateNormal];
        _startBuzzingButton.titleLabel.font = [UIFont appFontWithSize:50];
        [self addSubview:_startBuzzingButton];
    }
    return self;
}

- (void) layoutSubviews {
    _startBuzzingButton.frame = self.bounds;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
