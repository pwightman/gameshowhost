//
//  GSBoard.m
//  GameShowHost
//
//  Created by Parker Wightman on 3/13/13.
//  Copyright (c) 2013 Parker Wightman. All rights reserved.
//

#import "GSBoard.h"

@implementation GSBoard

+ (id) boardWithQuestionPack:(GSQuestionPack *)questionPack {
    GSBoard *board = [[GSBoard alloc] init];
    board.questionPack = questionPack;
    board.playedQuestions = [NSMutableArray array];
    
    NSMutableArray *played = [NSMutableArray arrayWithCapacity:questionPack.categoriesCount];
    
    for (NSInteger i = 0; i < questionPack.categoriesCount; i++) {
        NSMutableArray *columnPlayed = [NSMutableArray arrayWithCapacity:questionPack.questionsPerCategoryCount];
        
        for (NSInteger j = 0; j < questionPack.questionsPerCategoryCount; j++) {
            [columnPlayed addObject:@(NO)];
        }
        
        [played addObject:columnPlayed];
    }
    
    board.playedQuestions = played;
    
    return board;
}

@end
