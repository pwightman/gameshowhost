//
//  GSScoreChangeViewController.m
//  GameShowHost
//
//  Created by Parker Wightman on 3/15/13.
//  Copyright (c) 2013 Parker Wightman. All rights reserved.
//

#import "GSScoreChangeViewController.h"

@interface GSScoreChangeViewController ()
@property (weak, nonatomic) IBOutlet UITableView *tableView;

@end

@implementation GSScoreChangeViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}

- (NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return _game.players.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell"];
    
    cell.textLabel.text = [[_game.players objectAtIndex:indexPath.row] name];
    
    cell.detailTextLabel.text = [NSString stringWithFormat:@"%@", @([[_game.players objectAtIndex:indexPath.row] score])];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    GSPlayer *player = [_game.players objectAtIndex:indexPath.row];
    PSPDFAlertView *alertView = [[PSPDFAlertView alloc] initWithTitle:@"Score Change"
                                                              message:[NSString stringWithFormat:@"What is %@'s new score?", player.name]];
    
    alertView.alertViewStyle = UIAlertViewStylePlainTextInput;
    
    [alertView addButtonWithTitle:@"Change" extendedBlock:^(PSPDFAlertView *alert, NSInteger buttonIndex) {
        NSInteger newScore = [[alert textFieldAtIndex:0].text integerValue];
        [_game broadcastAndChangeScore:newScore forPlayer:player];
    }];
    
    [alertView setCancelButtonWithTitle:@"Cancel" block:^{ }];
    
    if (_didFinish)
        _didFinish();
    
    [alertView show];
}

- (IBAction)cancelTapped:(id)sender {
    if ( _didFinish )
        _didFinish();
}

@end
