//
//  GSPlayerNetworkNotifier.m
//  GameShowHost
//
//  Created by Parker Wightman on 3/11/13.
//  Copyright (c) 2013 Parker Wightman. All rights reserved.
//

#import "GSPlayerNetworkNotifier.h"

@interface GSPlayerNetworkNotifier () <DTBonjourDataConnectionDelegate>

@end

@implementation GSPlayerNetworkNotifier

+ (id) notifierWithConnection:(DTBonjourDataConnection *)connection {
    GSPlayerNetworkNotifier *notifier = [[GSPlayerNetworkNotifier alloc] init];
    
    notifier.connection = connection;
    // TODO: This could be bad.
    connection.delegate = notifier;
    
    return notifier;
}


/**
 Called when the connection did received and decode an object
 @param connection The connection
 @param object The decoded object that was received
 */
- (void)connection:(DTBonjourDataConnection *)connection didReceiveObject:(id)object {
    GSProtocol protocol = [[object objectForKey:@"protocol"] integerValue];
    
    if ( protocol == GSProtocolSignOut ) {
        [_player.delegate playerDidSignOut:_player];
        [_connection close];
    } else if ( protocol == GSProtocolBuzzIn ) {
        [_player.delegate playerDidBuzzIn:_player];
    }
}

/**
 Called when the connection was closed
 @param connection The connection
 */
- (void)connectionDidClose:(DTBonjourDataConnection *)connection {
    [_player.delegate playerDidDisconnect:_player];
}

- (void) sendGameBegan {
    NSError *error = nil;
    [_connection sendObject:@{ @"protocol": @(GSProtocolGameBegan) } error:&error];
    
    if ( error ) {
        NSLog(@"Error: send game began to %@ failed", _player.name);
    }
}

- (void) sendPlayersUpdate:(NSArray *)players {
    NSMutableArray *array = [NSMutableArray arrayWithCapacity:players.count];
    
    for (GSPlayer *player in players) {
        [array addObject:@{
           @"name":  player.name,
           @"id":    player.identifier,
           @"score": @(player.score)
         }];
    }
    
    NSError *error = nil;
    [_connection sendObject:@{ @"protocol" : @(GSProtocolPlayersUpdate), @"players": array } error:&error];
     
     if ( error ) {
        NSLog(@"Error: failed to send player update to players");
    }
}


- (void) sendWonBuzzing {
    [_connection sendObject:@{ @"protocol": @(GSProtocolWonBuzzIn) } error:nil];
}

- (void) sendBuzzedEarly {
    [_connection sendObject:@{ @"protocol": @(GSProtocolBuzzedEarly) } error:nil];
}


@end