//
//  GSGameBuzzedInView.h
//  GameShowHost
//
//  Created by Parker Wightman on 3/13/13.
//  Copyright (c) 2013 Parker Wightman. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GSGameBuzzedInView : UIView

@property (strong, nonatomic) UILabel *buzzedInPlayerLabel;
@property (strong, nonatomic) UIButton *correctButton;
@property (strong, nonatomic) UIButton *incorrectButton;


@end
