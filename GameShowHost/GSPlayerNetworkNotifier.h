//
//  GSPlayerNetworkNotifier.h
//  GameShowHost
//
//  Created by Parker Wightman on 3/11/13.
//  Copyright (c) 2013 Parker Wightman. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "GSPlayer.h"
#import "GSProtocol.h"
#import <DTBonjour/DTBonjourDataConnection.h>

@interface GSPlayerNetworkNotifier : NSObject <GSPlayerNotifier>

@property (strong, nonatomic) DTBonjourDataConnection *connection;
@property (strong, nonatomic) GSPlayer *player;

+ (id) notifierWithConnection:(DTBonjourDataConnection *)connection;

@end
