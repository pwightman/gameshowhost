//
//  GSBoardView.h
//  GameShowHost
//
//  Created by Parker Wightman on 3/13/13.
//  Copyright (c) 2013 Parker Wightman. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GSBoard.h"

@protocol GSBoardViewDelegate;

@interface GSBoardView : UIView

@property (weak, nonatomic) id<GSBoardViewDelegate> delegate;
@property (weak, nonatomic) GSBoard *board;

- (id) initWithBoard:(GSBoard *)board;

@end


@protocol GSBoardViewDelegate <NSObject>

- (BOOL)boardView:(GSBoardView *)boardView shouldShowQuestionInCategory:(NSInteger)category question:(NSInteger)question;

- (void) boardView:(GSBoardView *)boardView questionSelectedAtCategory:(NSInteger)category question:(NSInteger)question;

@end