//
//  GSQuestionSequenceViewController.h
//  GameShowHost
//
//  Created by Parker Wightman on 3/13/13.
//  Copyright (c) 2013 Parker Wightman. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GSGame.h"
#import "GSCategory.h"
#import "GSQuestion.h"
#import "GSGameQuestionSequence.h"
#import "GSExternalInGameViewController.h"

@interface GSQuestionSequenceViewController : UIViewController

@property (strong, nonatomic) GSGameQuestionSequence *sequence;

@property (weak, nonatomic) GSExternalInGameViewController *externalInGameController;

//@property (weak, nonatomic) GSGame *game;
//@property (weak, nonatomic) GSQuestion *question;
//@property (weak, nonatomic) GSCategory *category;
//
//@property (weak, nonatomic) GSPlayer *playerWithBoard;

@property (strong, nonatomic) void (^didFinishWithPlayerHavingBoard)(GSPlayer *playerWithBoard);

@end
